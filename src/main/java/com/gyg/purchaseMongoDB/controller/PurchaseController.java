package com.gyg.purchaseMongoDB.controller;

import com.gyg.purchaseMongoDB.record.ClientCountRecord;
import com.gyg.purchaseMongoDB.record.ClientRecord;
import com.gyg.purchaseMongoDB.record.MovieRecord;
import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import com.gyg.purchaseMongoDB.service.PurchaseService;
import com.gyg.purchaseMongoDB.service.api.PurchaseServiceApi;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("mongodb/purchases")
@AllArgsConstructor
@PreAuthorize("hasRole('admin-client')")
public class PurchaseController {

    private final PurchaseServiceApi purchaseService;

    @GetMapping("/")
    public ResponseEntity<Map<String, Object>> findPurchasesByHigherAmount() {
        Map<String, Object> map = mapResponse();
        List<PurchaseRecord> purchaseRecords = purchaseService.findPurchasesByHigherAmountLastWeek();
        map.put("purchases", purchaseRecords);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/client/{id}")
    public ResponseEntity<Map<String, Object>> findByClient(@PathVariable String id, Pageable pageable) {
        Map<String, Object> map = mapResponse();
        map.put("purchases", purchaseService.findPageableClient(id, pageable));
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Map<String, Object>> saveMovie(@Valid @RequestBody PurchaseRecord purchaseRecord) {
        purchaseService.save(purchaseRecord);
        Map<String, Object> map = mapResponse();
        return new ResponseEntity<>(map, HttpStatus.OK);
    }


    @GetMapping("/client")
    public ResponseEntity<Map<String, Object>> findClientsWhoMadeTheMostPurchases() {
        Map<String, Object> map = mapResponse();
        List<ClientCountRecord> purchaseRecords = purchaseService.findClientsWhoMadeTheMostPurchases();
        map.put("purchases", purchaseRecords);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    private Map<String, Object> mapResponse() {
        Map<String, Object> map = new HashMap<>();
        map.put("httpStatus", HttpStatus.OK);
        map.put("message", "La operacion se realizo con exito");
        return map;
    }
}
