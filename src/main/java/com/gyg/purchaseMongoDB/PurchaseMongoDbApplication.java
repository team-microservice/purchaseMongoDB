package com.gyg.purchaseMongoDB;

import com.gyg.purchaseMongoDB.record.ClientRecord;
import com.gyg.purchaseMongoDB.record.MovieRecord;
import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import com.gyg.purchaseMongoDB.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableMongoRepositories
//public class PurchaseMongoDbApplication {
//    @Autowired
//    PurchaseService purchaseService;
//
//    public static void main(String[] args) {
//        SpringApplication.run(PurchaseMongoDbApplication.class, args);
//
//        MovieRecord movie = new MovieRecord(1L, "iron man", "imagen", 100);
//        List<MovieRecord> movies = new ArrayList<>();
//        movies.add(movie);
//        ClientRecord client = new ClientRecord("1", "nico", "gomez", "nico@gmail.com");
//        PurchaseRecord purchaseRecord = new PurchaseRecord(1L, movies, client, 100, 90, LocalDate.now());
//        purchaseService.save(purchaseRecord);
//    }
public class PurchaseMongoDbApplication {

    @Autowired
    private PurchaseService purchaseService;

    public static void main(String[] args) {
        SpringApplication.run(PurchaseMongoDbApplication.class, args);
    }

//    @Override
//    public void run(String... args) throws Exception {
//  ***** Cargar ventas
//        MovieRecord movie = new MovieRecord(1L, "iron man", "imagen", 100);
//        List<MovieRecord> movies = new ArrayList<>();
//        movies.add(movie);
//        ClientRecord client = new ClientRecord("1", "nico", "gomez", "nico@gmail.com");
//        PurchaseRecord purchaseRecord = new PurchaseRecord(1L, movies, client, 100, 90, LocalDate.now());
//        purchaseService.save(purchaseRecord);
//
//        MovieRecord movie2 = new MovieRecord(2L, "iron man2", "imagen", 100);
//        List<MovieRecord> movies2 = new ArrayList<>();
//        movies2.add(movie2);
//        PurchaseRecord purchaseRecord2 = new PurchaseRecord(2L, movies2, client, 100, 90, LocalDate.now());
//        purchaseService.save(purchaseRecord2);
//
//        MovieRecord movie3 = new MovieRecord(3L, "iron man3", "imagen", 100);
//        List<MovieRecord> movies3 = new ArrayList<>();
//        movies3.add(movie3);
//        PurchaseRecord purchaseRecord3 = new PurchaseRecord(3L, movies3, client, 100, 90, LocalDate.now());
//        purchaseService.save(purchaseRecord3);

//****Listar ventas paginadas
//        purchaseService.findPageable("1", 0, 10, "ASC", "id").getContent().forEach(p -> System.out.println(p));
//    }

}
