package com.gyg.purchaseMongoDB.repository;

import com.gyg.purchaseMongoDB.model.ClientCount;
import com.gyg.purchaseMongoDB.model.Purchase;
import com.gyg.purchaseMongoDB.record.ClientCountRecord;
import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


@Repository
public interface PurchaseRepository extends MongoRepository<Purchase, String> {
    @Query("{'client.id':?0}")
    Page<Purchase> findByClientIdPageable(String clientId, Pageable pageable);

    @Query(value = "{'client.id': ?0}", exists = true)
//busca la compra por id de cliente y retorna un booleano
    boolean existsByClientId(String clientId);


    @Aggregation(pipeline = {
            "{$group: { _id: '$client._id', totalPurchases: { $sum: 1 }, name: { $first: '$client.name' }, surname: { $first: '$client.surname' }, email: { $first: '$client.email' }} }",
            "{$sort: { totalPurchases: -1 } }",
            "{$limit: 5 }"
    })
    List<ClientCount> findTop10ClientsByTotalPurchases();

    @Aggregation({
            "{$match: {date: {$gte: ?0}}}",
            "{$sort: {totalWithDiscount: -1}}",
            "{$limit: 10}"
    })
    List<Purchase> findPurchasesByHigherAmountAndDate(LocalDate date);
}
