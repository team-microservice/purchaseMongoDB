package com.gyg.purchaseMongoDB.model;

import com.gyg.purchaseMongoDB.record.ClientRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class Client {
    @Id
    String id;
    String name;
    String surname;
    String email;

    public ClientRecord toRecord() {
        return new ClientRecord(this.id, this.name, this.surname, this.email);
    }

}
