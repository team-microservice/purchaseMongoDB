package com.gyg.purchaseMongoDB.model;

import com.gyg.purchaseMongoDB.record.ClientCountRecord;
import lombok.Data;

@Data
public class ClientCount {

    private String id;
    private int totalPurchases;
    private String name;
    private String surname;
    private String email;

    public ClientCountRecord toRecord() {
        return new ClientCountRecord(id, totalPurchases, name, surname, email);
    }
}
