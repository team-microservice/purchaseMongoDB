package com.gyg.purchaseMongoDB.model;

import com.gyg.purchaseMongoDB.record.MovieRecord;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie {
    @Id
    private Long id;

    private String title;
    private String image;
    private double price;

    public MovieRecord toRecord() {
        return new MovieRecord(this.id, this.title, this.image, this.price);
    }


}
