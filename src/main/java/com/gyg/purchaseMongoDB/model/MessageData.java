package com.gyg.purchaseMongoDB.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Data;


import java.time.LocalDate;
import java.util.List;


@Data
public class MessageData {
    Long purchaseId;
    String clientId;
    String clientName;
    String clientSurname;

    private String email;
    private double total;
    private double totalWithDiscount;
    private List<Movie> moviesList;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

}
