package com.gyg.purchaseMongoDB.model;

import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import jakarta.persistence.ManyToMany;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "purchases")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Purchase {
    @Id
    Long id;
    Client client;
    double total;
    double totalWithDiscount;

    List<Movie> moviesList;

    LocalDate date;

    public PurchaseRecord toRecord() {
        return new PurchaseRecord(id, moviesList.stream().map(Movie::toRecord).collect(Collectors.toList()),
                client.toRecord(), total, totalWithDiscount, date);
    }

}
