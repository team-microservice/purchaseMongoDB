package com.gyg.purchaseMongoDB.service.api.exceptions;

import lombok.Data;
import org.springframework.http.HttpStatus;


@Data
public class NotExistException extends RuntimeException {
    HttpStatus httpStatus = HttpStatus.NOT_FOUND;
    String field;

    public NotExistException(String message, String field) {
        super(message);
        this.field = field;
    }
}
