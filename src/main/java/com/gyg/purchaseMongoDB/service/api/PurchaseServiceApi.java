package com.gyg.purchaseMongoDB.service.api;

import com.gyg.purchaseMongoDB.record.ClientCountRecord;
import com.gyg.purchaseMongoDB.record.ClientRecord;
import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PurchaseServiceApi {
    void save(PurchaseRecord purchaseRecord);

    //-- listar las ventas paginadas de un cliente
    Page<PurchaseRecord> findPageableClient(String clientId, Pageable pageable);

    //-- listar las últimas 10 ventas de mayor monto
    List<PurchaseRecord> findPurchasesByHigherAmountLastWeek();

    //-- listar los clientas que más compras hicieron
    List<ClientCountRecord> findClientsWhoMadeTheMostPurchases();
}
