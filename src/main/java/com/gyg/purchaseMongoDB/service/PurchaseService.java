package com.gyg.purchaseMongoDB.service;

import com.gyg.purchaseMongoDB.model.Client;
import com.gyg.purchaseMongoDB.model.Movie;
import com.gyg.purchaseMongoDB.model.Purchase;
import com.gyg.purchaseMongoDB.record.ClientCountRecord;
import com.gyg.purchaseMongoDB.record.PurchaseRecord;
import com.gyg.purchaseMongoDB.repository.PurchaseRepository;
import com.gyg.purchaseMongoDB.service.api.PurchaseServiceApi;
import com.gyg.purchaseMongoDB.service.api.exceptions.NotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PurchaseService implements PurchaseServiceApi {
    @Autowired
    PurchaseRepository purchaseRepository;

    @Override
    public void save(PurchaseRecord purchaseRecord) {
        List<Movie> movies = new ArrayList<>();
        purchaseRecord.moviesList().forEach(m -> movies.add(new Movie(m.id(), m.title(), m.image(), m.price())));
        Client client = new Client(
                purchaseRecord.client().id(),
                purchaseRecord.client().name(),
                purchaseRecord.client().surname(),
                purchaseRecord.client().email());
        Purchase purchase = new Purchase(purchaseRecord.id(),
                client,
                purchaseRecord.total(),
                purchaseRecord.totalWithDiscount(),
                movies,
                purchaseRecord.date());
        purchaseRepository.save(purchase);

    }

    @Override
    public Page<PurchaseRecord> findPageableClient(String clientId, Pageable pageable) {
        //VALIDAR QUE EXISTA UN CLIENTE
        if (!purchaseRepository.existsByClientId(clientId)) {
            throw new NotExistException("El id del cliente no existe", "clientId");
        }
        return purchaseRepository.findByClientIdPageable(clientId, pageable).map(p -> p.toRecord());
    }

    @Override
    public List<PurchaseRecord> findPurchasesByHigherAmountLastWeek() {
        List<PurchaseRecord> listPurchases = new ArrayList<>();
        LocalDate date = LocalDate.now().minusDays(7);
        listPurchases = purchaseRepository.findPurchasesByHigherAmountAndDate(date).stream().map(purchase ->
                        purchase.toRecord())
                .collect(Collectors.toList());
        ;
        return listPurchases;
    }

    @Override
    public List<ClientCountRecord> findClientsWhoMadeTheMostPurchases() {
        List<ClientCountRecord> listClient = new ArrayList<>();
        listClient = purchaseRepository.findTop10ClientsByTotalPurchases().stream().map(client ->
                        client.toRecord())
                .collect(Collectors.toList());
        ;
        return listClient;
    }
}
