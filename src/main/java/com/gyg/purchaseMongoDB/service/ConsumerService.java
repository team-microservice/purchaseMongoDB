package com.gyg.purchaseMongoDB.service;


import com.gyg.purchaseMongoDB.model.Client;
import com.gyg.purchaseMongoDB.model.MessageData;

import com.gyg.purchaseMongoDB.model.Movie;
import com.gyg.purchaseMongoDB.model.Purchase;
import com.gyg.purchaseMongoDB.repository.PurchaseRepository;
import com.gyg.purchaseMongoDB.service.api.exceptions.EmailException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ConsumerService {


    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerService.class);
    @Autowired
    private PurchaseRepository purchaseRepository;

    @RabbitListener(queues = {"${rabbitmq.queue.json.name}"})
    public void receive(MessageData messageData) throws EmailException {
        LOGGER.info("Received JSON message {}", messageData.toString());

        try {
            Client client = new Client(messageData.getClientId(), messageData.getClientName(), messageData.getClientSurname(), messageData.getEmail());
            List<Movie> movies = new ArrayList<>();
            messageData.getMoviesList().forEach(m -> movies.add(new Movie(m.getId(), m.getTitle(), m.getImage(), m.getPrice())));
            Purchase purchase = new Purchase(messageData.getPurchaseId(), client, messageData.getTotal(), messageData.getTotalWithDiscount(), movies, messageData.getDate());
            purchaseRepository.save(purchase);
            LOGGER.info("SAVE PURCHASE IN  MONGODB");
        } catch (MessagingException messagingException) {
            throw new EmailException(messagingException.getMessage());
        }
    }
}
