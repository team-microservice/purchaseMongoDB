package com.gyg.purchaseMongoDB.record;

public record ClientCountRecord(String id,
                                int totalPurchases,
                                String name,
                                String surname,
                                String email) {
}
