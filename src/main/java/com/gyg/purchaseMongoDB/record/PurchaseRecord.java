package com.gyg.purchaseMongoDB.record;


import java.time.LocalDate;
import java.util.List;

public record PurchaseRecord(Long id,
                             List<MovieRecord> moviesList,
                             ClientRecord client,
                             double total,
                             double totalWithDiscount,
                             LocalDate date) {
}
