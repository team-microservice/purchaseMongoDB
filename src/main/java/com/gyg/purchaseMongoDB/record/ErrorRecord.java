package com.gyg.purchaseMongoDB.record;

import org.springframework.http.HttpStatus;

public record ErrorRecord(HttpStatus httpStatus, String message) {

}
