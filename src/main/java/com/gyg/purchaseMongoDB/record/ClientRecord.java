package com.gyg.purchaseMongoDB.record;

public record ClientRecord(
        String id,
        String name,
        String surname,
        String email) {
}
