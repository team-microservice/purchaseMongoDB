package com.gyg.purchaseMongoDB.record;

public record MovieRecord(
        Long id,

        String title,

        String image,

        double price) {
}
